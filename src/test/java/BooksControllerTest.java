
import com.etav.controllers.BooksController;
import com.etav.model.Book;
import com.etav.services.BooksService;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class BooksControllerTest {
    
    @Mock
    BooksService booksService;

    @Test
    public void testwhenBookDoesNotContainPart_listBooksMatchingPartDoesNotIncludeIt() {
        //Arrange
        BooksController booksController = new BooksController(booksService);
        Book book = new Book();
        book.setTitle("example title");

        Mockito.when(
                booksService.findAll()
        ).thenReturn(
                Arrays.asList(new Book[] {book})
        );

        //Act
        List<Book> result = booksController.listBooksMatchingPart("xyz");

        //Assert
        assertEquals(0, result.size());
    }
    
    @Test
    public void testwhenBookContainsPart_listBooksMatchingPartIncludesIt() {
        //Arrange
        BooksController booksController = new BooksController(booksService);
        Book book = new Book();
        book.setTitle("example title");

        Mockito.when(
                booksService.findAll()
        ).thenReturn(
                Arrays.asList(new Book[] {book})
        );

        //Act
        List<Book> result = booksController.listBooksMatchingPart("ple");

        //Assert
        assertEquals(1, result.size());
        assertEquals(book, result.get(0));
    }
    
}