
import com.etav.model.Book;
import com.etav.model.Order;
import com.etav.services.OrdersService;
import com.etav.services.exceptions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {

    @Mock
    EntityManager em;

    @Test(expected = OutOfStockException.class)
    public void testwhenOrderedBookNotAvailable_placeOrderThrowsOutOfStockEx() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(0);
        book.setPrice(new Float(14.5));
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    @Test
    public void testwhenOrderedBookAvailable_placeOrderDecreasesAmountByOne() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        book.setPrice(new Float(31.5));
  
        
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }
    
        @Test
    public void testwhenOrderedBooksAvailable_placeOrderDecreasesAmountByNumberOfOrderedBooks() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        Book book2 = new Book();
        book.setAmount(2);
        book2.setAmount(3);
        book.setPrice(new Float(14.5));
        book2.setPrice(new Float(14.5));
        order.getBooks().add(book);
        order.getBooks().add(book);
        order.getBooks().add(book2);
        order.getBooks().add(book2);
        order.getBooks().add(book2);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)book.getAmount());
        assertEquals(0, (int)book2.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }
    
    @Test(expected = EmptyOrderException.class)
    public void testwhenOrderIsEmpty_placeOrderThrowsEmptyOrderException() {
        //Arrange
        Order order = new Order();
        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }
    
        @Test(expected = EmptyOrderException.class)
    public void testwhenOrderIsNull_placeOrderThrowsEmptyOrderException() {
        //Arrange
        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(null);

        //Assert - exception expected
    }
    
    @Test(expected = TooLowPriceException.class)
    public void testwhenOrderTotalPriceIsBelow30_placeOrderThrowsTooLowPriceException() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        Book book2 = new Book();
        book.setAmount(1);
        book2.setAmount(1);
        book.setPrice(new Float(14.5));
        book2.setPrice(new Float(14.5));
        order.getBooks().add(book);
        order.getBooks().add(book2);
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);
  
        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }
    
    
    @Test
    public void testwhenGivenLowercaseString_toUpperReturnsUppercase() {

        //Arrange
        String lower = "abcdef";

        //Act
        String result = lower.toUpperCase();

        //Assert
        assertEquals("ABCDEF", result);
    }
}
