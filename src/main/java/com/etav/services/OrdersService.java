package com.etav.services;

import com.etav.model.Book;
import com.etav.model.Order;
import com.etav.services.exceptions.*;
import java.util.ArrayList;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;
import javax.persistence.TypedQuery;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na zamówieniach.
 */
@Service
public class OrdersService extends EntityService<Order> {

    //Instancja klasy EntityManger zostanie dostarczona przez framework Spring
    //(wstrzykiwanie zależności przez konstruktor).
    public OrdersService(EntityManager em) {

        //Order.class - klasa encyjna, na której będą wykonywane operacje
        //Order::getId - metoda klasy encyjnej do pobierania klucza głównego
        super(em, Order.class, Order::getId);
    }

    /**
     * Pobranie wszystkich zamówień z bazy danych.
     *
     * @return lista zamówień
     */
    public List<Order> findAll() {
        return em.createQuery("SELECT o FROM Order o", Order.class).getResultList();
    }
    
    
     /**
     * Pobranie zamówień zawierających książke o danym ID z bazy danych.
     *
     * @return lista zamówień
     */
    public List<Order> findOrderWithBookID(UUID id) {
        return em.createQuery("SELECT o FROM Order o WHERE :id MEMBER OF o.books", Order.class).setParameter("id", em.find(Book.class, id)).getResultList();
    }
    
     /**
     * Pobranie zamówień zawierających książke o danym tytule z bazy danych.
     *
     * @return lista zamówień
     */
    public List<Order> findOrderWithBookTitle(String title) {
        
        List<Order> orders = new ArrayList<>();
        for (Order order: em.createQuery("SELECT o FROM Order o", Order.class).getResultList()){
            for(Book book:order.getBooks()){
                if(book.getTitle().equals(title)){
                    orders.add(order);
                    break;
                }
            }
        }
        return orders;
    }

    /**
     * Złożenie zamówienia w sklepie.
     * <p>
     * Zamówienie jest akceptowane, jeśli wszystkie objęte nim produkty są dostępne (przynajmniej 1 sztuka). W wyniku
     * złożenia zamówienia liczba dostępnych sztuk produktów jest zmniejszana o jeden. Metoda działa w sposób
     * transakcyjny - zamówienie jest albo akceptowane w całości albo odrzucane w całości. W razie braku produktu
     * wyrzucany jest wyjątek OutOfStockException.
     *
     * @param order zamówienie do przetworzenia
     */
    @Transactional
    public void placeOrder(Order order) {
        if(order == null || order.getBooks().isEmpty())
            throw new EmptyOrderException();
        float totalPrice = 0;
        for (Book bookStub : order.getBooks()) {
            Book book = em.find(Book.class, bookStub.getId());

            if (book.getAmount() < 1) {
                //wyjątek z hierarchii RuntineException powoduje wycofanie transakcji (rollback)
                throw new OutOfStockException();
            }
            else
            {
                totalPrice += book.getPrice();
                int newAmount = book.getAmount() - 1;
                book.setAmount(newAmount);
            }
        }
        if(totalPrice < 30)
        {
            throw new TooLowPriceException();
        }
        
        //jeśli wcześniej nie został wyrzucony wyjątek OutOfStockException, zamówienie jest zapisywane w bazie danych
        save(order);
    }
}
