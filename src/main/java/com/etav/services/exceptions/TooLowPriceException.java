package com.etav.services.exceptions;

/**
 * Wyjątek sygnalizujący zbyt niską wartość zamówienia.
 *
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback).
 */
public class TooLowPriceException extends RuntimeException {
}
